import React, { useState } from 'react';

export const GameContext = React.createContext({
  screen: '',
  setPreScreen: () => {},
  setMainScreen: () => {},
  setPostScreen: () => {},
});

const GameContextProvider = (props) => {
  const [screen, setScreen] = useState('PRE');

  const preScreenHandler = () => {
    setScreen('PRE');
  };

  const mainScreenHandler = () => {
    setScreen('MAIN');
  };

  const postScreenHandler = () => {
    setScreen('POST');
  };

  return (
    <GameContext.Provider
      value={{
        screen: screen,
        setPreScreen: preScreenHandler,
        setMainScreen: mainScreenHandler,
        setPostScreen: postScreenHandler,
      }}>
      {props.children}
    </GameContext.Provider>
  );
};

export default GameContextProvider;
